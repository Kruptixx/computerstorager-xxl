﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompStorage
{
    public static class ServiceClass
    {
        public static string KeyValyeStringDefine(GPUType type)
        {
            // Maxwell = 1, Pascal, Haskell, Kepler
            string _final;
            switch (type)
            {
                case GPUType.Maxwell:
                    _final = "Maxwell";
                    break;
                case GPUType.Pascal:
                    _final = "Pascal";
                    break;
                case GPUType.Haskell:
                    _final = "Haskell";
                    break;
                case GPUType.Kepler:
                    _final = "Kepler";
                    break;
                default:
                    _final = "UNKNOWN GPU SOCKET TYPE";
                    break;
            }
            return _final;
        }
        public static string KeyValyeStringDefine(SATAType type)
        {
            // SATA1 = 1, SATA2, SATA3, SATA31, SATA32
            string _final;
            switch (type)
            {
                case SATAType.SATA1:
                    _final = "SATA 1";
                    break;
                case SATAType.SATA2:
                    _final = "SATA 2";
                    break;
                case SATAType.SATA3:
                    _final = "SATA 3";
                    break;
                case SATAType.SATA31:
                    _final = "SATA 3.1";
                    break;
                case SATAType.SATA32:
                    _final = "SATA 3.2";
                    break;
                default:
                    _final = "UNKNOWN SATA TYPE";
                    break;
            }
            return _final;
        }
        public static string KeyValyeStringDefine(RAMSocket type)
        {
            // SIMM = 1, RIMM, DIMM, DDR, DDR2, DDR3, DDR4
            string _final;
            switch (type)
            {
                case RAMSocket.SIMM:
                    _final = "SIMM";
                    break;
                case RAMSocket.RIMM:
                    _final = "RIMM";
                    break;
                case RAMSocket.DIMM:
                    _final = "DIMM";
                    break;
                case RAMSocket.DDR2:
                    _final = "DDR2";
                    break;
                case RAMSocket.DDR:
                    _final = "DDR";
                    break;
                case RAMSocket.DDR3:
                    _final = "DDR3";
                    break;
                case RAMSocket.DDR4:
                    _final = "DDR4";
                    break;
                default:
                    _final = "UNKNOWN RAM SOCKET TYPE";
                    break;
            }
            return _final;
        }
        public static string KeyValyeStringDefine(ODDType type)
        {
            //    CD_ROM = 1, CD_R, CD_RW, DVD_ROM, DVD_R, DVD_RW, Audio_P,
            //    BD_ROM, BD_R, BD_RE, BD_RDL, BD_REDL
            string _final;
            switch (type)
            {
                case ODDType.CD_ROM:
                    _final = "CD-ROM";
                    break;
                case ODDType.CD_R:
                    _final = "CD-R";
                    break;
                case ODDType.CD_RW:
                    _final = "CD-RW";
                    break;
                case ODDType.DVD_ROM:
                    _final = "DVD_ROM";
                    break;
                case ODDType.DVD_R:
                    _final = "DVD-R";
                    break;
                case ODDType.DVD_RW:
                    _final = "DVD-RW";
                    break;
                case ODDType.Audio_P:
                    _final = "Audio Player";
                    break;
                case ODDType.BD_ROM:
                    _final = "BD-ROM";
                    break;
                case ODDType.BD_R:
                    _final = "BD-R";
                    break;
                case ODDType.BD_RE:
                    _final = "BD-RE";
                    break;
                case ODDType.BD_RDL:
                    _final = "BD-RDL";
                    break;
                case ODDType.BD_REDL:
                    _final = "BD-REDL";
                    break;
                default:
                    _final = "UNKNOWN ODD TYPE";
                    break;
            }
            return _final;
        }
        public static string KeyValyeStringDefine(ExSocket type)
        {
            //    USB = 1, USB2, USB3, USB31, USB_TypeC, DisplayPort,
            //    MiniDisplayPort, VGA, Thunderbolt, DVI, HDMI, PCI,
            //    Jack35, PS2
            string _final;
            switch (type)
            {
                case ExSocket.USB:
                    _final = "USB";
                    break;
                case ExSocket.USB2:
                    _final = "USB 2.0";
                    break;
                case ExSocket.USB3:
                    _final = "USB 3.0";
                    break;
                case ExSocket.USB31:
                    _final = "USB 3.1";
                    break;
                case ExSocket.USB_TypeC:
                    _final = "USB Type C";
                    break;
                case ExSocket.DisplayPort:
                    _final = "Display Port";
                    break;
                case ExSocket.MiniDisplayPort:
                    _final = "Mini Display Port";
                    break;
                case ExSocket.VGA:
                    _final = "VGA";
                    break;
                case ExSocket.Thunderbolt:
                    _final = "Thunderbolt";
                    break;
                case ExSocket.DVI:
                    _final = "DVI";
                    break;
                case ExSocket.HDMI:
                    _final = "HDMI";
                    break;
                case ExSocket.PCI:
                    _final = "PCI";
                    break;
                case ExSocket.Jack35:
                    _final = "Jack 3.5mm";
                    break;
                case ExSocket.PS2:
                    _final = "PS/2";
                    break;
                default:
                    _final = "UNKNOWN ODD TYPE";
                    break;
            }
            return _final;
        }
        public static string KeyValyeStringDefine(ProcSocket type)
        {
            //     DIP = 1, PLCC, Socket1, Socket2, Socket3, Socket4,
            // Socket5, Socket6, Socket7, SuperSocket7, Socket8, Slot1,
            // Slot2, SocketNexGen, Socket587, SlotA, SlotB, Socket370,
            // SocketA, Socket423, SocketN, Socket495, PAC418, Socket603,
            // Socket604, Socket563, Socket754, Socket940, Socket479,
            // Socket939, SocketT, SocketM, SocketJ, SocketS1, SocketAM2,
            // SocketF, SocketL, SocketAM2Plus, SocketP, Socket441, SocketB,
            // SocketG1, SocketAM3, SocketH, SocketG34, SocketC32, LGA1248,
            // LGA1567, SocketH2, SocketR, SocketG2, SocketFM1, SocketFS1,
            // SocketAM3Plus, SocketFM2, SocketH3, SocketG3, SocketFM2Plus,
            // SocketAM1, LGA1151, SocketAM4
            string _final;
            switch (type)
            {
                case ProcSocket.DIP:
                    _final = "DIP";
                    break;
                case ProcSocket.PLCC:
                    _final = "PLCC";
                    break;
                case ProcSocket.Socket1:
                    _final = "Socket 1";
                    break;
                case ProcSocket.Socket2:
                    _final = "Socket 2";
                    break;
                case ProcSocket.Socket3:
                    _final = "Socket 3";
                    break;
                case ProcSocket.Socket4:
                    _final = "Socket 4";
                    break;
                case ProcSocket.Socket5:
                    _final = "Socket 5";
                    break;
                case ProcSocket.Socket6:
                    _final = "Socket 6";
                    break;
                case ProcSocket.Socket7:
                    _final = "Socket2";
                    break;
                case ProcSocket.SuperSocket7:
                    _final = "Super Socket 7";
                    break;
                case ProcSocket.Socket8:
                    _final = "Socket 8";
                    break;
                case ProcSocket.Slot1:
                    _final = "Slot 1";
                    break;
                case ProcSocket.Slot2:
                    _final = "Slot 2";
                    break;
                case ProcSocket.SocketNexGen:
                    _final = "Socket 463 / Socket NexGen";
                    break;
                case ProcSocket.Socket587:
                    _final = "Socket587";
                    break;
                case ProcSocket.SlotA:
                    _final = "Slot A";
                    break;
                case ProcSocket.SlotB:
                    _final = "Slot B";
                    break;
                case ProcSocket.Socket370:
                    _final = "Socket 370";
                    break;
                case ProcSocket.SocketA:
                    _final = "Socket 462 / Socket A";
                    break;
                case ProcSocket.Socket423:
                    _final = "Socket 423";
                    break;
                case ProcSocket.SocketN:
                    _final = "Socket 478 / Socket N";
                    break;
                case ProcSocket.Socket495:
                    _final = "Socket 495";
                    break;
                case ProcSocket.Socket603:
                    _final = "Socket 603";
                    break;
                case ProcSocket.PAC418:
                    _final = "PAC418";
                    break;
                case ProcSocket.Socket604:
                    _final = "Socket 604";
                    break;
                case ProcSocket.Socket754:
                    _final = "Socket 754";
                    break;
                case ProcSocket.Socket940:
                    _final = "Socket 940";
                    break;
                case ProcSocket.Socket479:
                    _final = "Socket 479";
                    break;
                case ProcSocket.Socket939:
                    _final = "Socket 939";
                    break;
                case ProcSocket.SocketT:
                    _final = "LGA 775 / Socket T";
                    break;
                case ProcSocket.SocketM:
                    _final = "Socket M";
                    break;
                case ProcSocket.SocketJ:
                    _final = "LGA 771 / Socket J";
                    break;
                case ProcSocket.SocketS1:
                    _final = "Socket S1";
                    break;
                case ProcSocket.SocketAM2:
                    _final = "Socket AM2";
                    break;
                case ProcSocket.SocketF:
                    _final = "Socket F";
                    break;
                case ProcSocket.SocketL:
                    _final = "Socket L / Socket 1207FX";
                    break;
                case ProcSocket.SocketAM2Plus:
                    _final = "Socket AM2+";
                    break;
                case ProcSocket.SocketP:
                    _final = "Socket P";
                    break;
                case ProcSocket.Socket441:
                    _final = "Socket 441";
                    break;
                case ProcSocket.SocketB:
                    _final = "LGA 1366 / Socket B";
                    break;
                case ProcSocket.SocketG1:
                    _final = "rPGA 988A / Socket G1";
                    break;
                case ProcSocket.SocketAM3:
                    _final = "Socket AM3";
                    break;
                case ProcSocket.SocketH:
                    _final = "LGA 1156 / Socket H";
                    break;
                case ProcSocket.SocketG34:
                    _final = "Socket G34";
                    break;
                case ProcSocket.SocketC32:
                    _final = "Socket C32";
                    break;
                case ProcSocket.LGA1248:
                    _final = "LGA 1248";
                    break;
                case ProcSocket.LGA1567:
                    _final = "LGA 1567";
                    break;
                case ProcSocket.SocketH2:
                    _final = "LGA 1155 / Socket H2";
                    break;
                case ProcSocket.SocketR:
                    _final = "LGA 2011 / Socket R";
                    break;
                case ProcSocket.SocketG2:
                    _final = "rPGA 988B / Socket G2";
                    break;
                case ProcSocket.SocketFM1:
                    _final = "Socket FM1";
                    break;
                case ProcSocket.SocketFS1:
                    _final = "Socket FS1";
                    break;
                case ProcSocket.SocketAM3Plus:
                    _final = "Socket AM3+";
                    break;
                case ProcSocket.SocketH3:
                    _final = "LGA 1150 / Socket H3";
                    break;
                case ProcSocket.SocketG3:
                    _final = "Socket G3";
                    break;
                case ProcSocket.SocketFM2Plus:
                    _final = "Socket FM2+";
                    break;
                case ProcSocket.SocketAM1:
                    _final = "Socket AM1";
                    break;
                case ProcSocket.SocketAM4:
                    _final = "Socket AM4";
                    break;
                case ProcSocket.LGA1151:
                    _final = "LGA 1151";
                    break;
                default:
                    _final = "UNKNOWN PROCESSOR SOCKET TYPE";
                    break;
            }
            return _final;
        }
    }
}
