﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompStorage
{
    public enum ProcSocket : byte
    {
        DIP = 1, PLCC, Socket1, Socket2, Socket3, Socket4,
        Socket5, Socket6, Socket7, SuperSocket7, Socket8, Slot1,
        Slot2, SocketNexGen, Socket587, SlotA, SlotB, Socket370,
        SocketA, Socket423, SocketN, Socket495, PAC418, Socket603,
        Socket604, Socket563, Socket754, Socket940, Socket479,
        Socket939, SocketT, SocketM, SocketJ, SocketS1, SocketAM2,
        SocketF, SocketL, SocketAM2Plus, SocketP, Socket441, SocketB,
        SocketG1, SocketAM3, SocketH, SocketG34, SocketC32, LGA1248,
        LGA1567, SocketH2, SocketR, SocketG2, SocketFM1, SocketFS1,
        SocketAM3Plus, SocketFM2, SocketH3, SocketG3, SocketFM2Plus,
        SocketAM1, LGA1151, SocketAM4, NULLSK
    }

    public sealed class Processor: GeneralParameters
    {
        public KeyValuePair<ProcSocket, string> socket;
        public string ProcSock { get { return this.socket.Value; } }
        private ushort frequency;
        public ushort Frequency { get { return this.frequency; } set { this.frequency = value; } }
        private byte cores;
        public byte Cores { get { return this.cores; } set { this.cores = value; } }
        private byte threads;
        public byte Threads { get { return this.threads; } set { this.threads = value; } }

        public Processor(Manufacturer man, ushort fq = 0, byte core = 0, byte th = 0, ProcSocket socket = ProcSocket.NULLSK,
            string sn = "N/A", string md = "N/A", string psocket = "")
        {
            serialNumber = sn.ToUpper();
            model = md;
            Manufact = man;
            if (psocket != "")
            {
                for (ProcSocket tp = ProcSocket.DIP; tp < ProcSocket.NULLSK; ++tp)
                {
                    if (ServiceClass.KeyValyeStringDefine(tp) == psocket)
                        this.socket = new KeyValuePair<ProcSocket, string>(tp, ServiceClass.KeyValyeStringDefine(tp));
                }
            }
            else if (psocket == null)
                this.socket = new KeyValuePair<ProcSocket, string>(ProcSocket.NULLSK, ServiceClass.KeyValyeStringDefine(ProcSocket.NULLSK));
            else
                this.socket = new KeyValuePair<ProcSocket, string>(socket, ServiceClass.KeyValyeStringDefine(socket));
            frequency = fq;
            cores = core;
            threads = th;

            man.AddItem(this);
        }

        public override string ToString()
        {
            return String.Format("Serial Number: {0} | Model: {1} | Manufacturer: {2} | Socket: {3} | Frequency: {4} GHz | Cores: {5} | Threads: {6}",
                this.serialNumber, this.model, this.Manufact.ToString(), this.socket.Value, this.frequency.ToString(), this.cores.ToString(),
                this.threads.ToString());
        }
    }

    public class StorageProc : GeneralCollection<Processor>
    {
        #region ctors
        public StorageProc()
        {
            objArray = new Processor[0];
        }

        public StorageProc(params Processor[] array)
        {
            objArray = array;
        }
        #endregion

        public override Processor GetItem(string id)
        {
            foreach (Processor obj in objArray)
            {
                if (obj.SerialNumber.ToLower() == id.ToLower())
                    return obj;
            }

            return null;
        }

        public override Processor FindBy(string param, string str = "", ushort inter = 0)
        {
            switch (param.ToLower())
            {
                case "freq":
                    foreach (Processor item in objArray)
                    {
                        if (item.Frequency == inter)
                        {
                            return item;
                        }
                    }
                    break;
                case "core":
                    foreach (Processor item in objArray)
                    {
                        if (item.Cores == inter)
                        {
                            return item;
                        }
                    }
                    break;
                case "thread":
                    foreach (Processor item in objArray)
                    {
                        if (item.Threads == inter)
                        {
                            return item;
                        }
                    }
                    break;
                default:
                    break;
            }
            return null;
        }

        public override void SortBy(string sort, ref bool error)
        {
            error = false;
            switch (sort.ToLower())
            {
                case "freq":
                    for (int i = 0; i < objArray.Length; ++i)
                    {
                        for (int j = 0; j < objArray.Length; ++j)
                        {
                            if (objArray[i].Frequency > objArray[j].Frequency)
                            {
                                Processor c = objArray[i];
                                objArray[i] = objArray[j];
                                objArray[j] = c;
                            }
                        }
                    }
                    break;
                case "core":
                    for (int i = 0; i < objArray.Length; ++i)
                    {
                        for (int j = 0; j < objArray.Length; ++j)
                        {
                            if (objArray[i].Cores > objArray[j].Cores)
                            {
                                Processor c = objArray[i];
                                objArray[i] = objArray[j];
                                objArray[j] = c;
                            }
                        }
                    }
                    break;
                case "thread":
                    for (int i = 0; i < objArray.Length; ++i)
                    {
                        for (int j = 0; j < objArray.Length; ++j)
                        {
                            if (objArray[i].Threads > objArray[j].Threads)
                            {
                                Processor c = objArray[i];
                                objArray[i] = objArray[j];
                                objArray[j] = c;
                            }
                        }
                    }
                    break;
                default:
                    error = true;
                    break;
            }

            RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        protected override bool? IsExist(string id)
        {
            try
            {
                foreach (Processor obj in objArray)
                {
                    if (obj.SerialNumber.ToLower() == id.ToLower())
                        return true;
                }

                return false;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public override void Clear()
        {
            foreach (Processor item in objArray)
                item.Manufact.RemoveItem(item);

            Array.Clear(objArray, 0, objArray.Length);
            Array.Resize(ref objArray, 0);

            RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        public override bool Remove(Processor item)
        {
            try
            {
                item.Manufact.RemoveItem(item);
                int index = 0;
                foreach (Processor obj in objArray)
                {
                    if (obj.Equals(item))
                    {
                        Array.Clear(objArray, index, 1);
                        for (int i = index; i < objArray.Length - 1; ++i)
                            objArray[i] = objArray[i + 1];
                        Array.Resize(ref objArray, objArray.Length - 1);

                        RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));

                        return true;
                    }
                    index++;
                }

                return false;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool Remove(Processor item, StorageMB storage)
        {
            try
            {
                item.Manufact.RemoveItem(item);
                storage.Refresh(item);
                int index = 0;
                foreach (Processor obj in objArray)
                {
                    if (obj.Equals(item))
                    {
                        Array.Clear(objArray, index, 1);
                        for (int i = index; i < objArray.Length - 1; ++i)
                            objArray[i] = objArray[i + 1];
                        Array.Resize(ref objArray, objArray.Length - 1);

                        RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));

                        return true;
                    }
                    index++;
                }

                return false;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public override bool Remove(int index)
        {
            try
            {
                objArray[index].Manufact.RemoveItem(objArray[index]);
                Array.Clear(objArray, index, 1);
                for (int i = index; i < objArray.Length - 1; ++i)
                    objArray[i] = objArray[i + 1];
                Array.Resize(ref objArray, objArray.Length - 1);

                RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }

}
