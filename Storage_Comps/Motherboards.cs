﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompStorage
{
    public enum ExSocket : byte
    {
        USB = 1, USB2, USB3, USB31, USB_TypeC, DisplayPort,
        MiniDisplayPort, VGA, Thunderbolt, DVI, HDMI, PCI,
        Jack35, PS2, NULLSK
    }

    public sealed class Motherboard: GeneralParameters
    {
        public KeyValuePair<ProcSocket, string> procSocket;
        public string ProcSock { get { return this.procSocket.Value; } }
        private Processor proc;
        public Processor Proc { get { return this.proc; } set { this.proc = value.socket.Key != procSocket.Key ? proc : value; } }
        public string ProcStr { get { string a; return a = this.proc != null ? this.proc.Model : "No Processor"; } }
        public KeyValuePair<RAMSocket, string> ramSocket;
        public string RamSock { get { return this.ramSocket.Value; } }
        private RAMModule ramModule;
        public RAMModule RAM { get { return this.ramModule; } set { this.ramModule = value.ramSocket.Key != ramSocket.Key ? ramModule : value; } }
        public string RAMStr { get { string a; return a = this.ramModule != null ? this.ramModule.Model : "No RAM"; } }
        public byte ramAmount;
        public byte RAMAmount { get { return this.ramAmount; } set { this.ramAmount = value; } }
        public byte ramMaxAmount;
        public byte RAMMaxAmount { get { return this.ramMaxAmount; } set { this.ramMaxAmount = value; } }
        private VideoCard video;
        public VideoCard Video { get { return this.video; } set { this.video = value; } }
        public string VideoStr { get { string a; return a = this.video != null ? this.video.Model : "No Videocard"; } }
        private Dictionary<ExSocket, byte> exSocketAmount = new Dictionary<ExSocket, byte>();
        private Dictionary<ExSocket, string> exSocketName = new Dictionary<ExSocket, string>();
        public string ExSK { get { return this.ExSocketsToString(); } }

        public Motherboard(Manufacturer man, Dictionary<ExSocket, byte> exs, Processor proc = null, RAMModule ram = null, VideoCard vc = null, 
            byte rama = 0, byte ramm = 0, RAMSocket rams = RAMSocket.NULLSK, ProcSocket procs = ProcSocket.NULLSK, string sn = "N/A", 
            string md = "N/A", string psock = "", string rsock = "")
        {
            serialNumber = sn.ToUpper();
            model = md;
            Manufact = man;
            if (psock != "")
            {
                for (ProcSocket tp = ProcSocket.DIP; tp < ProcSocket.NULLSK; ++tp)
                {
                    if (ServiceClass.KeyValyeStringDefine(tp) == psock)
                        procSocket = new KeyValuePair<ProcSocket, string>(tp, ServiceClass.KeyValyeStringDefine(tp));
                }
            }
            else if (psock == null)
                procSocket = new KeyValuePair<ProcSocket, string>(ProcSocket.NULLSK, ServiceClass.KeyValyeStringDefine(ProcSocket.NULLSK));
            else
                procSocket = new KeyValuePair<ProcSocket, string>(procs, ServiceClass.KeyValyeStringDefine(procs));

            this.proc = proc.socket.Key != procSocket.Key ? null : proc;

            if (rsock != "")
            {
                for (RAMSocket tp = RAMSocket.SIMM; tp < RAMSocket.NULLSK; ++tp)
                {
                    if (ServiceClass.KeyValyeStringDefine(tp) == rsock)
                        ramSocket = new KeyValuePair<RAMSocket, string>(tp, ServiceClass.KeyValyeStringDefine(tp));
                }
            }
            else if (rsock == null)
                ramSocket = new KeyValuePair<RAMSocket, string>(RAMSocket.NULLSK, ServiceClass.KeyValyeStringDefine(RAMSocket.NULLSK));
            else
                ramSocket = new KeyValuePair<RAMSocket, string>(rams, ServiceClass.KeyValyeStringDefine(rams));
            ramModule = ram.ramSocket.Key != ramSocket.Key ? null : ram;
            if (ramModule == null)
                rama = 0;
            RAMMaxAmount = ramm;
            RAMAmount = ramm < rama ? ramm : rama;
            Video = vc; 
            exSocketAmount = exs;
            foreach (KeyValuePair<ExSocket, byte> pair in exSocketAmount)
                exSocketName.Add(pair.Key, ServiceClass.KeyValyeStringDefine(pair.Key));

            man.AddItem(this);
        }

        public string ExSocketsToString()
        {
            string _string = "";
            foreach (KeyValuePair<ExSocket, string> pair in exSocketName)
            {
                _string += pair.Value;
                _string += " - ";
                foreach (KeyValuePair<ExSocket, byte> pairz in exSocketAmount)
                {
                    if (pair.Key == pairz.Key)
                        _string += pairz.Value.ToString();
                }
                _string += ", ";
            }

            return _string;
        }

        public override string ToString()
        {
            string procparam = proc != null ? this.proc.Model : "No Processor";
            string ramparam = ramModule != null ? this.ramModule.Model : "No RAM";
            string videoparam = video != null ? this.video.Model : "No Videocard";

            return String.Format("Serial Number: {0} | Model: {1} | Manufacturer: {2} | ProcSocket: {3} | Processor: {4} | " +
            "RAM Socket: {5} | RAM Module: {6} | RAM Amount: {7} | Max RAM Amount: {8} | Videocard: {9} | External Ports: {10}",
                this.serialNumber, this.model, this.Manufact.ToString(), this.procSocket.Value, procparam, this.ramSocket.Value,
                ramparam, this.RAMAmount.ToString(), this.RAMMaxAmount.ToString(), videoparam, this.ExSocketsToString());
        }
    }

    public class StorageMB : GeneralCollection<Motherboard>
    {
        #region ctors
        public StorageMB()
        {
            objArray = new Motherboard[0];
        }

        public StorageMB(params Motherboard[] array)
        {
            objArray = array;
        }
        #endregion

        public override void Clear()
        {
            foreach (Motherboard item in objArray)
                item.Manufact.RemoveItem(item);

            Array.Clear(objArray, 0, objArray.Length);
            Array.Resize(ref objArray, 0);

            RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        public override Motherboard FindBy(string param, string str = "", ushort inta = 0)
        {
            switch (param.ToLower())
            {
                case "rammax":
                    foreach (Motherboard item in objArray)
                    {
                        if (item.RAMMaxAmount == inta)
                            return item;
                    }
                    break;
                case "rammodel":
                    foreach (Motherboard item in objArray)
                    {
                        if (item.RAM.Model == str)
                            return item;
                    }
                    break;
                case "procmodel":
                    foreach (Motherboard item in objArray)
                    {
                        if (item.Proc.Model == str)
                            return item;
                    }
                    break;
                case "videomodel":
                    foreach (Motherboard item in objArray)
                    {
                        if (item.Video.Model == str)
                            return item;
                    }
                    break;
                default:
                    break;
            }
            return null;
        }

        public override void SortBy(string sort, ref bool error)
        {
            error = false;
            switch (sort.ToLower())
            {
                case "videobuffer":
                    for (int i = 0; i < objArray.Length; ++i)
                    {
                        for (int j = 0; j < objArray.Length; ++j)
                        {
                            if (objArray[i].Video.Buffer > objArray[j].Video.Buffer)
                            {
                                Motherboard c = objArray[i];
                                objArray[i] = objArray[j];
                                objArray[j] = c;
                            }
                        }
                    }
                    break;
                case "videospeed":
                    for (int i = 0; i < objArray.Length; ++i)
                    {
                        for (int j = 0; j < objArray.Length; ++j)
                        {
                            if (objArray[i].Video.Speed > objArray[j].Video.Speed)
                            {
                                Motherboard c = objArray[i];
                                objArray[i] = objArray[j];
                                objArray[j] = c;
                            }
                        }
                    }
                    break;
                default:
                    error = true;
                    break;
            }

            RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        protected override bool? IsExist(string id)
        {
            try
            {
                foreach (Motherboard obj in objArray)
                {
                    if (obj.SerialNumber.ToLower() == id.ToLower())
                        return true;
                }

                return false;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        #region Refresh method
        public void Refresh(object obj)
        {
            // foreach ?
            if (obj.GetType() == typeof(VideoCard))
            {
                    foreach (Motherboard item in objArray)
                    {
                        if (item.Video == obj)
                            item.Video = null;
                    }
            }
            if (obj.GetType() == typeof(Processor))
            {
                    foreach (Motherboard item in objArray)
                    {
                        if (item.Proc == obj)
                            item.Proc = null;
                    }
            }
            if (obj.GetType() == typeof(RAMModule))
            {
                    foreach (Motherboard item in objArray)
                    {
                        if (item.RAM == obj)
                        {
                            item.RAM = null;
                            item.RAMAmount = 0;
                        }
                    }
            }

            RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }
        #endregion

        #region Remove method
        public override bool Remove(Motherboard item)
        {
            try
            {
                item.Manufact.RemoveItem(item);
                int index = 0;
                foreach (Motherboard obj in objArray)
                {
                    if (obj.Equals(item))
                    {
                        Array.Clear(objArray, index, 1);
                        for (int i = index; i < objArray.Length - 1; ++i)
                            objArray[i] = objArray[i + 1];
                        Array.Resize(ref objArray, objArray.Length - 1);

                        RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));

                        return true;
                    }
                    index++;
                }

                return false;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public override bool Remove(int index)
        {
            try
            {
                objArray[index].Manufact.RemoveItem(objArray[index]);
                Array.Clear(objArray, index, 1);
                for (int i = index; i < objArray.Length - 1; ++i)
                    objArray[i] = objArray[i + 1];
                Array.Resize(ref objArray, objArray.Length - 1);

                RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        #endregion
    }

}
