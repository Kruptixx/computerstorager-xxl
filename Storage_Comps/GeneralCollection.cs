﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Collections.Specialized;

namespace CompStorage
{
    public abstract class GeneralCollection<T> : ICollection<T>, IEnumerable<T>, IComparable<T>, INotifyCollectionChanged
    {
        protected T[] objArray = new T[0];
        public T[] ObjCollection { get { return this.objArray; } set { this.objArray = value; } }
        public event NotifyCollectionChangedEventHandler CollectionChanged;

        public virtual int CompareTo(T other)
        {
            if (this.Equals(other))
            {
                return 1;
            } else {
                return 0;
            }
        }

        public virtual void Add(T item)
        {
            Array.Resize(ref objArray, objArray.Length + 1);
            objArray[objArray.Length - 1] = item;

            RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        public virtual void Add(T item, string id)
        {
            if (IsExist(id) == false)
            {
                Array.Resize(ref objArray, objArray.Length + 1);
                objArray[objArray.Length - 1] = item;
            }

            RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        public virtual void Clear()
        {
            Array.Clear(objArray, 0, objArray.Length);
            Array.Resize(ref objArray, 0);

            RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        public virtual bool Contains(T item)
        {
            foreach(T obj in objArray)
            {
                if (obj.Equals(item))
                    return true;
            }
            return false;
        }

        public virtual void Reverse()
        {
            Array.Reverse(objArray);

            RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        public abstract void SortBy(string sort, ref bool error);
        public virtual T FindBy(string param, string str = "", ushort inta = 0) 
        { 
            return default(T);
        }

        public virtual void CopyTo(T[] array, int arrayIndex)
        {
            int oldLength = objArray.Length;
            Array.Resize(ref objArray, objArray.Length+array.Length);
            for (int i = oldLength; i > arrayIndex; i--)
                objArray[i+array.Length] = objArray[i];
            Array.Copy(array, 0, objArray, arrayIndex, array.Length);

            RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        public virtual T GetItem(int index)
        {
            try
            {
                return objArray[index];
            }
            catch(Exception e)
            {
                return default(T);
            }
        }

        public virtual T GetItem(string id)
        {
            return default(T);
        }

        protected virtual bool? IsExist(string id)
        {
            return null;
        }

        public virtual int Count
        {
            get { return objArray.Count(); }
        }

        public virtual bool? IsReadOnly
        {
            get { return null; }
        }

        bool ICollection<T>.IsReadOnly
        {
            get { return false; }
        }

        public virtual bool Remove(T item)
        {
            try
            {
                int index = 0;
                foreach (T obj in objArray)
                {
                    if (obj.Equals(item))
                    {
                        Array.Clear(objArray, index, 1);
                        for (int i = index; i < objArray.Length - 1; ++i)
                            objArray[i] = objArray[i + 1];
                        Array.Resize(ref objArray, objArray.Length - 1);

                        RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));

                        return true;
                    }
                    index++;
                }

                return false;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public virtual bool Remove(int index)
        {
            try
            {
                Array.Clear(objArray, index, 1);
                for (int i = index; i < objArray.Length - 1; ++i)
                    objArray[i] = objArray[i + 1];
                Array.Resize(ref objArray, objArray.Length - 1);

                RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public virtual IEnumerator<T> GetEnumerator()
        {
            for (int i = 0; i < objArray.Length; ++i)
                yield return objArray[i];
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        protected virtual void RaiseCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            var handler = CollectionChanged;
            if (handler != null)
                handler(this, e);
        }
    }

    public abstract class GeneralParameters
    {
        protected string serialNumber;
        public string SerialNumber { get { return this.serialNumber; } set { this.serialNumber = value; } }
        protected string model;
        public string Model { get { return this.model; } set { this.model = value; } }
        public Manufacturer Manufact;
        public string ManufacturStr { get { return Manufact.ToString(); } }
        public string FullInfo { get { return this.ToString(); } }
    }
}
