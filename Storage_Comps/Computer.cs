﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Specialized;
using System.Reflection;

namespace CompStorage
{
    public class Computer
    {
        private string identificationNumber;
        public string IdentificationNumber { get { return this.identificationNumber; } set { this.identificationNumber = value; } }
        private Motherboard mainboard;
        public string Mainboard { get { return mainboard.Model; } }
        private PowerSupply powerSupply;
        public string PowerSupply { get { return powerSupply.Model; } }
        private HardDiskDrive hdd;
        public string HDD { get { return hdd.Model; } }
        private OpticalDiscDrive odd;
        public string ODD { get { return odd.Model; } }
        private Processor proc;
        public string Processor { get { return proc.Model; } }
        private RAMModule ram;
        public string RAM { get { return ram.Model; } }
        private VideoCard video;
        public string Video { get { return video.Model; } }

        public Computer(Motherboard mb, PowerSupply ps, HardDiskDrive hdd, OpticalDiscDrive odd, string id = "N/A")
        {
            identificationNumber = id.ToUpper();
            mainboard = mb;
            powerSupply = ps;
            this.hdd = hdd;
            this.odd = odd;
            proc = mb.Proc;
            ram = mb.RAM;
            video = mb.Video;
        }

        public override string ToString()
        {
            string procparam = proc != null ? this.proc.Model : "No Processor";
            string ramparam = ram != null ? this.ram.Model : "No RAM";
            string videoparam = video != null ? this.video.Model : "No Videocard";
            string motherparam = mainboard != null ? this.mainboard.Model : "No Motherboard";
            string powerparam = powerSupply != null ? this.powerSupply.Model : "No Power Supply";
            string hddparam = hdd != null ? this.hdd.Model : "No HDD";
            string oddparam = odd != null ? this.odd.Model : "No ODD";
            return String.Format("ID: {0} | Mainboard: {1} | Power Supply: {2} | HDD: {3} | ODD: {4} | Processor: {5} | RAM: {6} | Videocard: {7}",
                this.identificationNumber, motherparam, powerparam, hddparam, oddparam, procparam, ramparam, videoparam);
        }
    }

    public class Storage : GeneralCollection<Computer>
    {
        #region ctors
        public Storage()
        {
            objArray = new Computer[0];
        }
        #endregion

        public override void SortBy(string sort, ref bool error) 
        {

        }

        public override Computer FindBy(string param, string str = "", ushort inta = 0)
        {
            return null;
        }

        protected override bool? IsExist(string id)
        {
            try
            {
                foreach (Computer obj in objArray)
                {
                    if (obj.IdentificationNumber.ToLower() == id.ToLower())
                        return true;
                }

                return false;
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }

}
