﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompStorage
{
    public enum GPUType : byte
    {
        Maxwell = 1, Pascal, Haskell, Kepler, NULLSK
    }

    public sealed class VideoCard: GeneralParameters
    {
        public KeyValuePair<GPUType, string> gpu;
        public string GPU { get { return this.gpu.Value; } }
        private byte buffer;
        public byte Buffer { get { return this.buffer; } set { this.buffer = value; } }
        private ushort speed;
        public ushort Speed { get { return this.speed; } set { this.speed = value; } }

        public VideoCard(Manufacturer man, byte buf = 0, ushort speed = 0, GPUType type = GPUType.NULLSK, 
            string sn = "N/A", string md = "N/A", string gtype = "")
        {
            serialNumber = sn.ToUpper();
            model = md;
            Manufact = man;
            buffer = buf;
            this.speed = speed;
            if (gtype != "" && gtype != null)
            {
                for (GPUType tp = GPUType.Maxwell; tp < GPUType.NULLSK; ++tp)
                {
                    if(ServiceClass.KeyValyeStringDefine(tp) == gtype)
                        gpu = new KeyValuePair<GPUType, string>(tp, ServiceClass.KeyValyeStringDefine(tp));
                }
            }
            else if(gtype == null)
                gpu = new KeyValuePair<GPUType, string>(GPUType.NULLSK, ServiceClass.KeyValyeStringDefine(GPUType.NULLSK));
            else
                gpu = new KeyValuePair<GPUType, string>(type, ServiceClass.KeyValyeStringDefine(type));

            man.AddItem(this);
        }

        public override string ToString()
        {
            return String.Format("Serial Number: {0} | Model: {1} | Manufacturer: {2} | GPU Type: {3} | Buffer: {4} Gb | Speed: {5}",
                this.serialNumber, this.model, this.Manufact.ToString(), this.gpu.Value, this.buffer.ToString(), this.speed.ToString());
            //return "Serial Number: " + this.SerialNumber + " | Model: " + this.Model + " | Manufacturer: " + this.Manufact.ToString() +
            //" | GPU Type: " + this.GPU.Value + " | Buffer: " + this.Buffer.ToString() + " Gb | Speed: " + this.Speed.ToString();
        }
    }

    public class StorageVC : GeneralCollection<VideoCard>
    {
        #region ctors
        public StorageVC()
        {
            objArray = new VideoCard[0];
        }

        public StorageVC(params VideoCard[] array)
        {
            objArray = array;
        }
        #endregion

        public override void Clear()
        {
            foreach (VideoCard item in objArray)
                item.Manufact.RemoveItem(item);

            Array.Clear(objArray, 0, objArray.Length);
            Array.Resize(ref objArray, 0);

            RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        public override VideoCard GetItem(string id)
        {
            foreach (VideoCard obj in objArray)
            {
                if (obj.SerialNumber.ToLower() == id.ToLower())
                    return obj;
            }

            return null;
        }

        public override VideoCard FindBy(string param, string str = "", ushort inta = 0)
        {
            switch (param.ToLower())
            {
                case "buffer":
                    foreach (VideoCard item in objArray)
                    {
                        if(item.Buffer == (byte)inta)
                            return item;
                    }
                    break;
                case "speed":
                    foreach (VideoCard item in objArray)
                    {
                        if (item.Speed == inta)
                            return item;
                    }
                    break;
                default:
                    break;
            }
            return null;
        }

        public override void SortBy(string sort, ref bool error)
        {
            error = false;
            switch (sort.ToLower())
            { 
                case "buffer":
                    for (int i = 0; i < objArray.Length; ++i)
                    {
                        for (int j = 0; j < objArray.Length; ++j)
                        {
                            if (objArray[i].Buffer > objArray[j].Buffer)
                            {
                                VideoCard c = objArray[i];
                                objArray[i] = objArray[j];
                                objArray[j] = c;
                            }
                        }
                    }
                    break;
                case "speed":
                    for (int i = 0; i < objArray.Length; ++i)
                    {
                        for (int j = 0; j < objArray.Length; ++j)
                        {
                            if (objArray[i].Speed > objArray[j].Speed)
                            {
                                VideoCard c = objArray[i];
                                objArray[i] = objArray[j];
                                objArray[j] = c;
                            }
                        }
                    }
                    break;
                default:
                    error = true;
                    break;
            }

            RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        protected override bool? IsExist(string id)
        {
            try
            {
                foreach (VideoCard obj in objArray)
                {
                    if (obj.SerialNumber.ToLower() == id.ToLower())
                        return true;
                }

                //if (id != null)
                //    return false;
                //else
                //    return true;

                return id != null ? false : true;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public override bool Remove(VideoCard item)
        {
            try
            {
                item.Manufact.RemoveItem(item);
                int index = 0;
                foreach (VideoCard obj in objArray)
                {
                    if (obj.Equals(item))
                    {
                        Array.Clear(objArray, index, 1);
                        for (int i = index; i < objArray.Length - 1; ++i)
                        {
                            objArray[i] = objArray[i + 1];
                        }
                        Array.Resize(ref objArray, objArray.Length - 1);

                        RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));

                        return true;
                    }
                    index++;
                }

                return false;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool Remove(VideoCard item, StorageMB storage)
        {
            try
            {
                item.Manufact.RemoveItem(item);
                storage.Refresh(item);
                int index = 0;
                foreach (VideoCard obj in objArray)
                {
                    if (obj.Equals(item))
                    {
                        Array.Clear(objArray, index, 1);
                        for (int i = index; i < objArray.Length - 1; ++i)
                            objArray[i] = objArray[i + 1];
                        Array.Resize(ref objArray, objArray.Length - 1);

                        RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));

                        return true;
                    }
                    index++;
                }

                return false;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public override bool Remove(int index)
        {
            try
            {
                objArray[index].Manufact.RemoveItem(objArray[index]);
                Array.Clear(objArray, index, 1);
                for (int i = index; i < objArray.Length - 1; ++i)
                    objArray[i] = objArray[i + 1];
                Array.Resize(ref objArray, objArray.Length - 1);

                RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }

}
