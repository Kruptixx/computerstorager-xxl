﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompStorage
{
    public enum ODDType : byte
    {
        CD_ROM = 1, CD_R, CD_RW, DVD_ROM, DVD_R, DVD_RW, Audio_P,
        BD_ROM, BD_R, BD_RE, BD_RDL, BD_REDL, NULLSK
    }

    public sealed class OpticalDiscDrive: GeneralParameters
    {
        public KeyValuePair<ODDType, string> oddType;
        public string ODDTypeStr { get { return this.oddType.Value; } }

        public OpticalDiscDrive(Manufacturer man, ODDType odd = ODDType.NULLSK, string sn = "N/A", string md = "N/A", string oddt = "")
        {
            serialNumber = sn.ToUpper();
            model = md;
            Manufact = man;
            if (oddt != "")
            {
                for (ODDType tp = ODDType.CD_ROM; tp < ODDType.NULLSK; ++tp)
                {
                    if (ServiceClass.KeyValyeStringDefine(tp) == oddt)
                        oddType = new KeyValuePair<ODDType, string>(tp, ServiceClass.KeyValyeStringDefine(tp));
                }
            }
            else if (oddt == null)
                oddType = new KeyValuePair<ODDType, string>(ODDType.NULLSK, ServiceClass.KeyValyeStringDefine(ODDType.NULLSK));
            else
                oddType = new KeyValuePair<ODDType, string>(odd, ServiceClass.KeyValyeStringDefine(odd));

            man.AddItem(this);
        }

        public override string ToString()
        {
            return String.Format("Serial Number: {0} | Model: {1} | Manufacturer: {2} | ODD Type: {3}",
                this.serialNumber, this.model, this.Manufact.ToString(), this.oddType.Value);
        }
    }

    public class StorageODD : GeneralCollection<OpticalDiscDrive>
    {
        #region ctors
        public StorageODD()
        {
            objArray = new OpticalDiscDrive[0];
        }

        public StorageODD(params OpticalDiscDrive[] array)
        {
            objArray = array;
        }
        #endregion

        public override OpticalDiscDrive FindBy(string param, string str = "", ushort inta = 0)
        {
            switch (param.ToLower())
            {
                case "oddtype":
                    foreach (OpticalDiscDrive item in objArray)
                    {
                        if (item.oddType.Value == str)
                        {
                            return item;
                        }
                    }
                    break;
                default:
                    break;
            }
            return null;
        }

        public override void SortBy(string sort, ref bool error)
        {
            error = true;

            RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        protected override bool? IsExist(string id)
        {
            try
            {
                foreach (OpticalDiscDrive obj in objArray)
                {
                    if (obj.SerialNumber.ToLower() == id.ToLower())
                        return true;
                }

                return false;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public override void Clear()
        {
            foreach (OpticalDiscDrive item in objArray)
            {
                item.Manufact.RemoveItem(item);
            }

            Array.Clear(objArray, 0, objArray.Length);
            Array.Resize(ref objArray, 0);

            RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        public override bool Remove(OpticalDiscDrive item)
        {
            try
            {
                item.Manufact.RemoveItem(item);
                int index = 0;
                foreach (OpticalDiscDrive obj in objArray)
                {
                    if (obj.Equals(item))
                    {
                        Array.Clear(objArray, index, 1);
                        for (int i = index; i < objArray.Length - 1; ++i)
                        {
                            objArray[i] = objArray[i + 1];
                        }
                        Array.Resize(ref objArray, objArray.Length - 1);

                        RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));

                        return true;
                    }
                    index++;
                }

                return false;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public override bool Remove(int index)
        {
            try
            {
                objArray[index].Manufact.RemoveItem(objArray[index]);
                Array.Clear(objArray, index, 1);
                for (int i = index; i < objArray.Length - 1; ++i)
                    objArray[i] = objArray[i + 1];
                Array.Resize(ref objArray, objArray.Length - 1);

                RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }

}
