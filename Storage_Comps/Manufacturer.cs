﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompStorage
{
    public sealed class Manufacturer
    {
        private string name;
        public List<Motherboard> Mainboard = new List<Motherboard>();
        public List<PowerSupply> PowerSup = new List<PowerSupply>();
        public List<HardDiskDrive> HDD = new List<HardDiskDrive>();
        public List<OpticalDiscDrive> ODD = new List<OpticalDiscDrive>();
        public List<Processor> Proc = new List<Processor>();
        public List<RAMModule> RAM = new List<RAMModule>();
        public List<VideoCard> Video = new List<VideoCard>();

        public Manufacturer(string name)
        {
            this.name = name;
        }

        public override string ToString()
        {
            return this.name;
        }

        public string Products()
        {
            string _final = "";
            if (Mainboard.Count != 0)
            {
                _final += @"Motherboards: ";
                foreach (Motherboard a in Mainboard)
                    _final += a.Model + ", ";
            }
            if (PowerSup.Count != 0)
            {
                _final += @"  Power Supplies: ";
                foreach (PowerSupply a in PowerSup)
                    _final += a.Model + ", ";
            }
            if (HDD.Count != 0)
            {
                _final += @"  HDDs: ";
                foreach (HardDiskDrive a in HDD)
                    _final += a.Model + ", ";
            }
            if (ODD.Count != 0)
            {
                _final += @"  ODDs: ";
                foreach (OpticalDiscDrive a in ODD)
                    _final += a.Model + ", ";
            }
            if (Proc.Count != 0)
            {
                _final += @"  Processors: ";
                foreach (Processor a in Proc)
                    _final += a.Model + ", ";
            }
            if (RAM.Count != 0)
            {
                _final += @"  RAM Modules: ";
                foreach (RAMModule a in RAM)
                    _final += a.Model + ", ";
            }
            if (Video.Count != 0)
            {
                _final += @"  Videocards: ";
                foreach (VideoCard a in Video)
                    _final += a.Model + ", ";
            }
            return String.Format("Name: {0} | {1}", this.name, _final);
        }

        public void AddItem(object obj)
        {
            if (obj.GetType() == typeof(Motherboard))
                Mainboard.Add((Motherboard)obj);
            if (obj.GetType() == typeof(PowerSupply))
                PowerSup.Add((PowerSupply)obj);
            if (obj.GetType() == typeof(HardDiskDrive))
                HDD.Add((HardDiskDrive)obj);
            if (obj.GetType() == typeof(OpticalDiscDrive))
                ODD.Add((OpticalDiscDrive)obj);
            if (obj.GetType() == typeof(Processor))
                Proc.Add((Processor)obj);
            if (obj.GetType() == typeof(RAMModule))
                RAM.Add((RAMModule)obj);
            if (obj.GetType() == typeof(VideoCard))
                Video.Add((VideoCard)obj);
        }

        public void RemoveItem(object obj)
        {
            if (obj.GetType() == typeof(Motherboard))
                Mainboard.Remove((Motherboard)obj);
            if (obj.GetType() == typeof(PowerSupply))
                PowerSup.Remove((PowerSupply)obj);
            if (obj.GetType() == typeof(HardDiskDrive))
                HDD.Remove((HardDiskDrive)obj);
            if (obj.GetType() == typeof(OpticalDiscDrive))
                ODD.Remove((OpticalDiscDrive)obj);
            if (obj.GetType() == typeof(Processor))
                Proc.Remove((Processor)obj);
            if (obj.GetType() == typeof(RAMModule))
                RAM.Remove((RAMModule)obj);
            if (obj.GetType() == typeof(VideoCard))
                Video.Remove((VideoCard)obj);
        }
    }
}
