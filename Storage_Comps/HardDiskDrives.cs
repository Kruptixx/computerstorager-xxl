﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;
using System.Collections.Specialized;
using System.Collections;

namespace CompStorage
{
    public enum SATAType : byte
    {
        SATA1 = 1, SATA2, SATA3, SATA31, SATA32, NULLSK
    }

    public sealed class HardDiskDrive: GeneralParameters
    {
        private ushort capacityGb;
        public ushort CapacityGb { get { return this.capacityGb; } set { this.capacityGb = value; } }
        private KeyValuePair<SATAType, string> sata;
        public string SATA { get { return this.sata.Value; } }

        public HardDiskDrive(Manufacturer man, SATAType type = SATAType.NULLSK, ushort gb = 0, string sn = "N/A", string md = "N/A",
            string stype = "")
        {
            serialNumber = sn.ToUpper();
            model = md;
            Manufact = man;
            capacityGb = gb;
            if (stype != "")
            {
                for (SATAType tp = SATAType.SATA1; tp < SATAType.NULLSK; ++tp)
                {
                    if (ServiceClass.KeyValyeStringDefine(tp) == stype)
                        sata = new KeyValuePair<SATAType, string>(tp, ServiceClass.KeyValyeStringDefine(tp));
                }
            }
            else if (stype == null)
                sata = new KeyValuePair<SATAType, string>(SATAType.NULLSK, ServiceClass.KeyValyeStringDefine(SATAType.NULLSK));
            else
                sata = new KeyValuePair<SATAType, string>(type, ServiceClass.KeyValyeStringDefine(type));

            man.AddItem(this);
        }

        public override string ToString()
        {
            return String.Format("Serial Number: {0} | Model: {1} | Manufacturer: {2} | SATA Type: {3} | CapacityGb: {4} Gb",
                this.serialNumber, this.model, this.Manufact.ToString(), this.sata.Value, this.CapacityGb.ToString());
        }

        public BigInteger CapacityToBytes()
        {
            return CapacityGb * 1000000000;
        }

        public BigInteger CapacityToMb()
        {
            return CapacityGb * 1000000;
        }

        public double CapacityToTb()
        {
            return CapacityGb / 1000;
        }

    }

    public class StorageHDD : GeneralCollection<HardDiskDrive>
    {
        #region ctors
        public StorageHDD()
        {
            objArray = new HardDiskDrive[0];
        }

        public StorageHDD(params HardDiskDrive[] array)
        {
            objArray = array;
        }
        #endregion

        public override void Clear()
        {
            foreach (HardDiskDrive item in objArray)
            {
                item.Manufact.RemoveItem(item);
            }

            Array.Clear(objArray, 0, objArray.Length);
            Array.Resize(ref objArray, 0);

            RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        public override HardDiskDrive FindBy(string param, string str = "", ushort inta = 0)
        {
            switch (param.ToLower())
            {
                case "capacity":
                    foreach (HardDiskDrive item in objArray)
                    {
                        if (item.CapacityGb == inta)
                        {
                            return item;
                        }
                    }
                    break;
                default:
                    break;
            }
            return null;
        }

        public override void SortBy(string sort, ref bool error)
        {
            error = false;
            switch (sort.ToLower())
            {
                case "capacity":
                    for (int i = 0; i < objArray.Length; ++i)
                    {
                        for (int j = 0; j < objArray.Length; ++j)
                        {
                            if (objArray[i].CapacityGb > objArray[j].CapacityGb)
                            {
                                HardDiskDrive c = objArray[i];
                                objArray[i] = objArray[j];
                                objArray[j] = c;
                            }
                        }
                    }
                    break;
                default:
                    error = true;
                    break;
            }

            RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        protected override bool? IsExist(string id)
        {
            try
            {
                foreach (HardDiskDrive obj in objArray)
                {
                    if (obj.SerialNumber.ToLower() == id.ToLower())
                        return true;
                }

                return false;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public override bool Remove(HardDiskDrive item)
        {
            try
            {
                item.Manufact.RemoveItem(item);
                int index = 0;
                foreach (HardDiskDrive obj in objArray)
                {
                    if (obj.Equals(item))
                    {
                        Array.Clear(objArray, index, 1);
                        for (int i = index; i < objArray.Length - 1; ++i)
                        {
                            objArray[i] = objArray[i + 1];
                        }
                        Array.Resize(ref objArray, objArray.Length - 1);

                        RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));

                        return true;
                    }
                    index++;
                }

                return false;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public override bool Remove(int index)
        {
            try
            {
                objArray[index].Manufact.RemoveItem(objArray[index]);
                Array.Clear(objArray, index, 1);
                for (int i = index; i < objArray.Length - 1; ++i)
                    objArray[i] = objArray[i + 1];
                Array.Resize(ref objArray, objArray.Length - 1);

                RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }

}
