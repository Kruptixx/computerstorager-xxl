﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompStorage
{
    public sealed class PowerSupply: GeneralParameters
    {
        private ushort power;
        public ushort Power { get { return this.power; } set { this.power = value; } }
        // 80Plus

        public PowerSupply(Manufacturer man, ushort power = 0, string sn = "N/A", string md = "N/A")
        {
            serialNumber = sn.ToUpper();
            model = md;
            Manufact = man;
            this.power = power;

            man.AddItem(this);
        }

        public override string ToString()
        {
            return String.Format("Serial Number: {0} | Model: {1} | Manufacturer: {2} | Power: {3} watt", 
                this.serialNumber, this.model, this.Manufact.ToString(), this.Power.ToString());
        }
    }

    public class StoragePS : GeneralCollection<PowerSupply>
    {
        #region ctors
        public StoragePS()
        {
            objArray = new PowerSupply[0];
        }

        public StoragePS(params PowerSupply[] array)
        {
            objArray = array;
        }
        #endregion

        public override PowerSupply FindBy(string param, string str = "", ushort inter = 0)
        {
            switch (param.ToLower())
            {
                case "power":
                    foreach (PowerSupply item in objArray)
                    {
                        if (item.Power == inter)
                        {
                            return item;
                        }
                    }
                    break;
                default:
                    break;
            }
            return null;
        }

        public override void SortBy(string sort, ref bool error)
        {
            error = false;
            switch (sort.ToLower())
            {
                case "power":
                    for (int i = 0; i < objArray.Length; ++i)
                    {
                        for (int j = 0; j < objArray.Length; ++j)
                        {
                            if (objArray[i].Power > objArray[j].Power)
                            {
                                PowerSupply c = objArray[i];
                                objArray[i] = objArray[j];
                                objArray[j] = c;
                            }
                        }
                    }
                    break;
                default:
                    error = true;
                    break;
            }

            RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        protected override bool? IsExist(string id)
        {
            try
            {
                foreach (PowerSupply obj in objArray)
                {
                    if (obj.SerialNumber.ToLower() == id.ToLower())
                        return true;
                }

                return false;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public override void Clear()
        {
            foreach (PowerSupply item in objArray)
            {
                item.Manufact.RemoveItem(item);
            }

            Array.Clear(objArray, 0, objArray.Length);
            Array.Resize(ref objArray, 0);

            RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        public override bool Remove(PowerSupply item)
        {
            try
            {
                item.Manufact.RemoveItem(item);
                int index = 0;
                foreach (PowerSupply obj in objArray)
                {
                    if (obj.Equals(item))
                    {
                        Array.Clear(objArray, index, 1);
                        for (int i = index; i < objArray.Length - 1; ++i)
                        {
                            objArray[i] = objArray[i + 1];
                        }
                        Array.Resize(ref objArray, objArray.Length - 1);

                        RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));

                        return true;
                    }
                    index++;
                }

                return false;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public override bool Remove(int index)
        {
            try
            {
                objArray[index].Manufact.RemoveItem(objArray[index]);
                Array.Clear(objArray, index, 1);
                for (int i = index; i < objArray.Length - 1; ++i)
                    objArray[i] = objArray[i + 1];
                Array.Resize(ref objArray, objArray.Length - 1);

                RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }

}
