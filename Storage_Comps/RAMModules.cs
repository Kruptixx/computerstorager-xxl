﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompStorage
{
    public enum RAMSocket : byte
    {
        SIMM = 1, RIMM, DIMM, DDR, DDR2, DDR3, DDR4, NULLSK
    }

    public sealed class RAMModule: GeneralParameters
    {
        public KeyValuePair<RAMSocket, string> ramSocket;
        public string RamSock { get { return this.ramSocket.Value; } }
        private ushort capacityGb;
        public ushort CapacityGb { get { return this.capacityGb; } set { this.capacityGb = value; } }
        private ushort speed;
        public ushort Speed { get { return this.speed; } set { this.speed = value; } }

        public RAMModule(Manufacturer man, ushort gb = 0, ushort speed = 0, RAMSocket socket = RAMSocket.NULLSK, 
            string sn = "N/A", string md = "N/A", string rsocket = "")
        {
            serialNumber = sn.ToUpper();
            model = md;
            Manufact = man;
            capacityGb = gb;
            this.speed = speed;
            if (rsocket != "")
            {
                for (RAMSocket tp = RAMSocket.SIMM; tp < RAMSocket.NULLSK; ++tp)
                {
                    if (ServiceClass.KeyValyeStringDefine(tp) == rsocket)
                        ramSocket = new KeyValuePair<RAMSocket, string>(tp, ServiceClass.KeyValyeStringDefine(tp));
                }
            }
            else if (rsocket == null)
                ramSocket = new KeyValuePair<RAMSocket, string>(RAMSocket.NULLSK, ServiceClass.KeyValyeStringDefine(RAMSocket.NULLSK));
            else
                ramSocket = new KeyValuePair<RAMSocket, string>(socket, ServiceClass.KeyValyeStringDefine(socket));

            man.AddItem(this);
        }

        public override string ToString()
        {
            return String.Format("Serial Number: {0} | Model: {1} | Manufacturer: {2} | Socket: {3} | Capacity: {4} Gb | Speed: {5}",
                this.serialNumber, this.model, this.Manufact.ToString(), this.ramSocket.Value, this.capacityGb.ToString(), this.speed.ToString());
        }
    }

    public class StorageRAM : GeneralCollection<RAMModule>
    {
        #region ctors
        public StorageRAM()
        {
            objArray = new RAMModule[0];
        }

        public StorageRAM(params RAMModule[] array)
        {
            objArray = array;
        }
        #endregion

        public override RAMModule GetItem(string id)
        {
            foreach (RAMModule obj in objArray)
            {
                if (obj.SerialNumber.ToLower() == id.ToLower())
                    return obj;
            }

            return null;
        }

        public override RAMModule FindBy(string param, string str = "", ushort inter = 0)
        {
            switch (param.ToLower())
            {
                case "capacity":
                    foreach (RAMModule item in objArray)
                    {
                        if (item.CapacityGb == inter)
                            return item;
                    }
                    break;
                case "speed":
                    foreach (RAMModule item in objArray)
                    {
                        if (item.Speed == inter)
                            return item;
                    }
                    break;
                default:
                    break;
            }
            return null;
        }

        public override void SortBy(string sort, ref bool error)
        {
            error = false;
            switch (sort.ToLower())
            {
                case "capacity":
                    for (int i = 0; i < objArray.Length; ++i)
                    {
                        for (int j = 0; j < objArray.Length; ++j)
                        {
                            if (objArray[i].CapacityGb > objArray[j].CapacityGb)
                            {
                                RAMModule c = objArray[i];
                                objArray[i] = objArray[j];
                                objArray[j] = c;
                            }
                        }
                    }
                    break;
                case "speed":
                    for (int i = 0; i < objArray.Length; ++i)
                    {
                        for (int j = 0; j < objArray.Length; ++j)
                        {
                            if (objArray[i].Speed > objArray[j].Speed)
                            {
                                RAMModule c = objArray[i];
                                objArray[i] = objArray[j];
                                objArray[j] = c;
                            }
                        }
                    }
                    break;
                default:
                    error = true;
                    break;
            }

            RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        protected override bool? IsExist(string id)
        {
            try
            {
                foreach (RAMModule obj in objArray)
                {
                    if (obj.SerialNumber.ToLower() == id.ToLower())
                        return true;
                }

                return false;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public override void Clear()
        {
            foreach (RAMModule item in objArray)
            {
                item.Manufact.RemoveItem(item);
            }

            Array.Clear(objArray, 0, objArray.Length);
            Array.Resize(ref objArray, 0);

            RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        public override bool Remove(RAMModule item)
        {
            try
            {
                item.Manufact.RemoveItem(item);
                int index = 0;
                foreach (RAMModule obj in objArray)
                {
                    if (obj.Equals(item))
                    {
                        Array.Clear(objArray, index, 1);
                        for (int i = index; i < objArray.Length - 1; ++i)
                            objArray[i] = objArray[i + 1];
                        Array.Resize(ref objArray, objArray.Length - 1);

                        RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));

                        return true;
                    }
                    index++;
                }

                return false;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool Remove(RAMModule item, StorageMB storage)
        {
            try
            {
                item.Manufact.RemoveItem(item);
                storage.Refresh(item);
                int index = 0;
                foreach (RAMModule obj in objArray)
                {
                    if (obj.Equals(item))
                    {
                        Array.Clear(objArray, index, 1);
                        for (int i = index; i < objArray.Length - 1; ++i)
                            objArray[i] = objArray[i + 1];
                        Array.Resize(ref objArray, objArray.Length - 1);

                        RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));

                        return true;
                    }
                    index++;
                }

                return false;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public override bool Remove(int index)
        {
            try
            {
                objArray[index].Manufact.RemoveItem(objArray[index]);
                Array.Clear(objArray, index, 1);
                for (int i = index; i < objArray.Length - 1; ++i)
                    objArray[i] = objArray[i + 1];
                Array.Resize(ref objArray, objArray.Length - 1);

                RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }

}
