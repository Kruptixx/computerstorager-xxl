﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CompStorage;
using System.Windows.Input;
using VM;

namespace WPF3Laba
{
    public class MWViewModel: ViewModelBase
    {
        public object SelectedVC { get; set; }
        public object SelectedMB { get; set; }
        public object SelectedPS { get; set; }
        public object SelectedComp { get; set; }
        public object SelectedHDD { get; set; }
        public object SelectedODD { get; set; }
        public object SelectedProc { get; set; }
        public object SelectedRAM { get; set; }

        #region Add parameters
        //videocard
        public string VCserial { get; set; }
        public string VCmodel { get; set; }
        public string VCbuffer { get; set; }
        public string VCspeed { get; set; }
        public object VCman { get; set; }
        public object VCgpu { get; set; }
        //power supply
        public string PSserial { get; set; }
        public string PSmodel { get; set; }
        public string PSpower { get; set; }
        public object PSman { get; set; }
        //ram
        public string RAMserial { get; set; }
        public string RAMmodel { get; set; }
        public string RAMcapacity { get; set; }
        public string RAMspeed { get; set; }
        public object RAMman { get; set; }
        public object RAMsocket { get; set; }
        //processor
        public string PROCserial { get; set; }
        public string PROCmodel { get; set; }
        public string PROCcores { get; set; }
        public string PROCthreads { get; set; }
        public string PROCfreq { get; set; }
        public object PROCman { get; set; }
        public object PROCsocket { get; set; }
        //odd
        public string ODDserial { get; set; }
        public string ODDmodel { get; set; }
        public object ODDman { get; set; }
        public object ODDtype { get; set; }
        //hdd
        public string HDDserial { get; set; }
        public string HDDmodel { get; set; }
        public string HDDcapacity { get; set; }
        public object HDDman { get; set; }
        public object HDDtype { get; set; }
        //motherboard
        public string MBserial { get; set; }
        public string MBmodel { get; set; }
        public string MBrama { get; set; }
        public string MBramm { get; set; }
        public object MBman { get; set; }
        public object MBproc { get; set; }
        public object MBvideo { get; set; }
        public object MBram { get; set; }
        public object MBptype { get; set; }
        public object MBrtype { get; set; }

        public string EXusb { get; set; }
        public string EXusb2 { get; set; }
        public string EXusb3 { get; set; }
        public string EXusb31 { get; set; }
        public string EXusbc { get; set; }
        public string EXdp { get; set; }
        public string EXmdp { get; set; }
        public string EXvga { get; set; }
        public string EXtbolt { get; set; }
        public string EXhdmi { get; set; }
        public string EXdvi { get; set; }
        public string EXpci { get; set; }
        public string EXjack { get; set; }
        public string EXps2 { get; set; }
        //comp
        #endregion

        private bool statusAddVC;
        public bool StatusAddVC { get { return statusAddVC; } set { statusAddVC = VCserial == "" ? false : true; 
            NotifyPropertyChanged("StatusAddVC"); } }

        public string SelectedItemInfo { get; set; }
        public string SelectedMBInfo { get; set; }

        public Storage CompStorage { get; set; }
        public StorageVC VCStorage { get; set; } 
        public StorageHDD HDDStorage { get; set; }
        public StorageMB MBStorage { get; set; }
        public StorageODD ODDStorage { get; set; }
        public StorageProc ProcStorage { get; set; }
        public StoragePS PSStorage { get; set; }
        public StorageRAM RAMStorage { get; set; }

        public Manufacturer[] ManArray { get; set; }
        public string[] GPUTypez { get; set; }
        public string[] RAMSocketz { get; set; }
        public string[] PROCSocketz { get; set; }
        public string[] ODDTypez { get; set; }
        public string[] SATATypez { get; set; }

        #region Commands
        #region videocard
        //videocard
        public ICommand ReverseVC
        {
            get
            {
                return new RelayCommand( () =>
                {
                    VCStorage.Reverse();
                    NotifyPropertyChanged("ReverseVC");
                });
            }
        }
        public ICommand RemoveVC
        {
            get
            {
                return new RelayCommand(() =>
                {
                    VCStorage.Remove((VideoCard)SelectedVC, MBStorage);
                    NotifyPropertyChanged("RemoveVC");
                });
            }
        }
        public ICommand AddVC
        {
            get
            {
                return new RelayCommand(() =>
                {
                    try
                    {
                        if (VCserial != "")
                        {
                            VCStorage.Add(new VideoCard((Manufacturer)VCman, sn: VCserial, md: VCmodel, gtype: (string)VCgpu,
                                buf: Convert.ToByte(VCbuffer), speed: Convert.ToUInt16(VCspeed)), VCserial);
                            VCserial = "";
                            NotifyPropertyChanged("VCserial");
                        }
                    }
                    catch (Exception e)
                    { }

                    NotifyPropertyChanged("AddVC");
                });
            }
        }
        #endregion

        #region power supply
        //power supply
        public ICommand RemovePS
        {
            get
            {
                return new RelayCommand(() =>
                {
                    PSStorage.Remove((PowerSupply)SelectedPS);
                    NotifyPropertyChanged("RemovePS");
                });
            }
        }
        public ICommand AddPS
        {
            get
            {
                return new RelayCommand(() =>
                {
                    try
                    {
                        if (PSserial != "")
                        {
                            PSStorage.Add(new PowerSupply((Manufacturer)PSman, Convert.ToUInt16(PSpower), PSserial, PSmodel), PSserial);
                            PSserial = "";
                            NotifyPropertyChanged("PSserial");
                        }
                    }
                    catch (Exception e)
                    { }

                    NotifyPropertyChanged("AddPS");
                });
            }
        }
        #endregion

        #region processor
        //proc
        public ICommand RemovePROC
        {
            get
            {
                return new RelayCommand(() =>
                {
                    ProcStorage.Remove((Processor)SelectedProc, MBStorage);
                    NotifyPropertyChanged("RemovePROC");
                });
            }
        }
        public ICommand AddPROC
        {
            get
            {
                return new RelayCommand(() =>
                {
                    try
                    {
                        if (PROCserial != "")
                        {
                            ProcStorage.Add(new Processor((Manufacturer)PROCman, fq: Convert.ToUInt16(PROCfreq), 
                                core: Convert.ToByte(PROCcores), th: Convert.ToByte(PROCthreads), sn: PROCserial, md: PROCmodel,
                                psocket: (string)PROCsocket), PROCserial);
                            PROCserial = "";
                            NotifyPropertyChanged("PROCserial");
                        }
                    }
                    catch (Exception e)
                    { }

                    NotifyPropertyChanged("AddPROC");
                });
            }
        }
        #endregion

        #region ram
        //ram
        public ICommand RemoveRAM
        {
            get
            {
                return new RelayCommand(() =>
                {
                    RAMStorage.Remove((RAMModule)SelectedRAM, MBStorage);
                    NotifyPropertyChanged("RemoveRAM");
                });
            }
        }
        public ICommand AddRAM
        {
            get
            {
                return new RelayCommand(() =>
                {
                    try
                    {
                        if (RAMserial != "")
                        {
                            RAMStorage.Add(new RAMModule((Manufacturer)RAMman, gb: Convert.ToUInt16(RAMcapacity), 
                                speed: Convert.ToUInt16(RAMspeed), sn: RAMserial, md: RAMmodel, rsocket: (string)RAMsocket), RAMserial);
                            RAMserial = "";
                            NotifyPropertyChanged("RAMserial");
                        }
                    }
                    catch (Exception e)
                    { }

                    NotifyPropertyChanged("AddRAM");
                });
            }
        }
        #endregion

        #region odd
        //odd
        public ICommand RemoveODD
        {
            get
            {
                return new RelayCommand(() =>
                {
                    ODDStorage.Remove((OpticalDiscDrive)SelectedODD);
                    NotifyPropertyChanged("RemoveODD");
                });
            }
        }
        public ICommand AddODD
        {
            get
            {
                return new RelayCommand(() =>
                {
                    try
                    {
                        if (ODDserial != "")
                        {
                            ODDStorage.Add(new OpticalDiscDrive((Manufacturer)ODDman, sn: ODDserial, md: ODDmodel, oddt: (string)ODDtype),
                                ODDserial);
                            ODDserial = "";
                            NotifyPropertyChanged("ODDserial");
                        }
                    }
                    catch (Exception e)
                    { }

                    NotifyPropertyChanged("AddODD");
                });
            }
        }
        #endregion

        #region hdd
        //hdd
        public ICommand RemoveHDD
        {
            get
            {
                return new RelayCommand(() =>
                {
                    HDDStorage.Remove((HardDiskDrive)SelectedHDD);
                    NotifyPropertyChanged("RemoveHDD");
                });
            }
        }
        public ICommand AddHDD
        {
            get
            {
                return new RelayCommand(() =>
                {
                    try
                    {
                        if (HDDserial != "")
                        {
                            HDDStorage.Add(new HardDiskDrive((Manufacturer)HDDman, gb: Convert.ToUInt16(HDDcapacity), sn: HDDserial,
                                md: HDDmodel, stype: (string)HDDtype), HDDserial);
                            HDDserial = "";
                            NotifyPropertyChanged("HDDserial");
                        }
                    }
                    catch (Exception e)
                    { }

                    NotifyPropertyChanged("AddHDD");
                });
            }
        }
        #endregion

        #region motherboard
        //motherboard
        public ICommand RemoveMB
        {
            get
            {
                return new RelayCommand(() =>
                {
                    MBStorage.Remove((Motherboard)SelectedMB);
                    NotifyPropertyChanged("RemoveMB");
                });
            }
        }
        public ICommand AddMB
        {
            get
            {
                return new RelayCommand(() =>
                {
                    try
                    {
                        Dictionary<ExSocket, byte> dict = new Dictionary<ExSocket, byte>();
                        if (Convert.ToByte(EXusb) > 0)
                            dict.Add(ExSocket.USB, Convert.ToByte(EXusb));
                        if (Convert.ToByte(EXusb2) > 0)
                            dict.Add(ExSocket.USB2, Convert.ToByte(EXusb2));
                        if (Convert.ToByte(EXusb3) > 0)
                            dict.Add(ExSocket.USB3, Convert.ToByte(EXusb3));
                        if (Convert.ToByte(EXusb31) > 0)
                            dict.Add(ExSocket.USB31, Convert.ToByte(EXusb31));
                        if (Convert.ToByte(EXusbc) > 0)
                            dict.Add(ExSocket.USB_TypeC, Convert.ToByte(EXusbc));
                        if (Convert.ToByte(EXdp) > 0)
                            dict.Add(ExSocket.DisplayPort, Convert.ToByte(EXdp));
                        if (Convert.ToByte(EXmdp) > 0)
                            dict.Add(ExSocket.MiniDisplayPort, Convert.ToByte(EXmdp));
                        if (Convert.ToByte(EXvga) > 0)
                            dict.Add(ExSocket.VGA, Convert.ToByte(EXvga));
                        if (Convert.ToByte(EXhdmi) > 0)
                            dict.Add(ExSocket.HDMI, Convert.ToByte(EXhdmi));
                        if (Convert.ToByte(EXtbolt) > 0)
                            dict.Add(ExSocket.Thunderbolt, Convert.ToByte(EXtbolt));
                        if (Convert.ToByte(EXdvi) > 0)
                            dict.Add(ExSocket.DVI, Convert.ToByte(EXdvi));
                        if (Convert.ToByte(EXpci) > 0)
                            dict.Add(ExSocket.PCI, Convert.ToByte(EXpci));
                        if (Convert.ToByte(EXps2) > 0)
                            dict.Add(ExSocket.PS2, Convert.ToByte(EXps2));
                        if (Convert.ToByte(EXjack) > 0)
                            dict.Add(ExSocket.Jack35, Convert.ToByte(EXjack));

                        if (MBserial != "")
                        {
                            MBStorage.Add(new Motherboard((Manufacturer)MBman, dict, proc: (Processor)MBproc, ram: (RAMModule)MBram,
                                vc: (VideoCard)MBvideo, rama: Convert.ToByte(MBrama), ramm: Convert.ToByte(MBramm), sn: MBserial,
                                md: MBmodel, psock: (string)MBptype, rsock: (string)MBrtype), MBserial);
                            MBserial = "";
                            NotifyPropertyChanged("MBserial");
                        }
                    }
                    catch (Exception e)
                    { }

                    NotifyPropertyChanged("AddMB");
                });
            }
        }
        #endregion

        #region comp
        //comp
        public ICommand RemoveCOMP
        {
            get
            {
                return new RelayCommand(() =>
                {
                    RAMStorage.Remove((RAMModule)SelectedRAM);
                    NotifyPropertyChanged("RemoveRAM");
                });
            }
        }
        public ICommand AddCOMP
        {
            get
            {
                return new RelayCommand(() =>
                {
                    try
                    {
                        if (RAMserial != "")
                        {
                            RAMStorage.Add(new RAMModule((Manufacturer)RAMman, gb: Convert.ToUInt16(RAMcapacity),
                                speed: Convert.ToUInt16(RAMspeed), sn: RAMserial, md: RAMmodel, rsocket: (string)RAMsocket), RAMserial);
                            RAMserial = "";
                            NotifyPropertyChanged("RAMserial");
                        }
                    }
                    catch (Exception e)
                    { }

                    NotifyPropertyChanged("AddRAM");
                });
            }
        }
        #endregion
        #endregion

        public MWViewModel()
        {
            Manufacturer mni = new Manufacturer("Intel");
            Manufacturer mna = new Manufacturer("ASUS");
            Manufacturer mns = new Manufacturer("Steel Series");
            Manufacturer mnc = new Manufacturer("Corsair");
            Manufacturer mnn = new Manufacturer("nVidia");
            ManArray = new Manufacturer[5];
            ManArray[0] = mni;
            ManArray[1] = mna;
            ManArray[2] = mns;
            ManArray[3] = mnc;
            ManArray[4] = mnn;

            StatusAddVC = true;

            EXusb = "0";
            EXusb2 = "0";
            EXusb3 = "0";
            EXusb31 = "0";
            EXusbc = "0";
            EXdp = "0";
            EXmdp = "0";
            EXvga = "0";
            EXtbolt = "0";
            EXhdmi = "0";
            EXdvi = "0";
            EXpci = "0";
            EXjack = "0";
            EXps2 = "0";
            MBrama = "0";
            MBramm = "0";

            CompStorage = new Storage();
            VCStorage = new StorageVC();
            HDDStorage = new StorageHDD();
            MBStorage = new StorageMB();
            ODDStorage = new StorageODD();
            ProcStorage = new StorageProc();
            PSStorage = new StoragePS();
            RAMStorage = new StorageRAM();

            VCStorage.Add(new VideoCard(mnn, md: "GeForce GTX 1080 SLI", type: GPUType.Pascal, sn: "NH1JA3", buf: 1, speed: 2000), "NH1JA3");
            VCStorage.Add(new VideoCard(mnn, md: "GeForce GTX 970Ti", type: GPUType.Maxwell, sn: "IIF56", buf: 21), "IIF56");
            VCStorage.Add(new VideoCard(mnc, md: "Hydro GFX GTX 1080", sn: "HFTE34", buf: 4), "HFTE34");
            VCStorage.Add(new VideoCard(mnn, md: "GeForce Titan Z", sn: "4ERYWGDH", buf: 3, speed: 3000), "4ERYWGDH");
            VCStorage.Add(new VideoCard(mnc, md: "GFX GTX 970", sn: "4EDDYFW", buf: 25), "4EDDYFW");
            VCStorage.Add(new VideoCard(mnn, md: "GeForce Titan X", sn: "AP89RYWFFW", buf: 3, speed: 100), "AP89RYWFFW");

            RAMModule ramnull = new RAMModule(mna);
            Processor procnull = new Processor(mna);

            MBStorage.Add(new Motherboard(mna, new Dictionary<ExSocket, byte>() {
                {ExSocket.HDMI, 1},
                {ExSocket.Jack35, 2},
                {ExSocket.Thunderbolt, 1},
                {ExSocket.USB3, 8}
            }, procnull, ramnull, vc: VCStorage.GetItem("4EDdyFW"), rama: 2, ramm: 8, rams: RAMSocket.DDR4, procs: ProcSocket.SocketAM4, sn: "56ATF89PP", md: "Rampage V"), "56ATF89PP");
            MBStorage.Add(new Motherboard(mna, new Dictionary<ExSocket, byte>() {
                {ExSocket.HDMI, 1},
                {ExSocket.Jack35, 3},
                {ExSocket.USB3, 3}
            }, procnull, ramnull, vc: VCStorage.GetItem("AP89RYWFFW"), rama: 8, ramm: 4, rams: RAMSocket.DDR3, procs: ProcSocket.SocketAM3, sn: "7yus879sf", md: "Rampage VI"), "7yus879SF");
            MBStorage.Add(new Motherboard(mns, new Dictionary<ExSocket, byte>() {
                {ExSocket.HDMI, 1},
                {ExSocket.Jack35, 2},
                {ExSocket.Thunderbolt, 1},
                {ExSocket.USB2, 4}
            }, procnull, ramnull, vc: VCStorage.GetItem("HFTE34"), rama: 0, ramm: 6, rams: RAMSocket.DDR2, procs: ProcSocket.SocketAM2, sn: "7hf92hoahfo", md: "Hyper GMM A9"), "7hf92hoahfo");

            #region Enums initialize
            GPUTypez = new string[5];
            RAMSocketz = new string[7];
            PROCSocketz = new string[61];
            ODDTypez = new string[12];
            SATATypez = new string[5];
            int i = 0;
            for (GPUType a = GPUType.Maxwell; a < GPUType.NULLSK; ++a)
            {
                GPUTypez[i] = ServiceClass.KeyValyeStringDefine(a);
                ++i;
            }
            i = 0;
            for (RAMSocket a = RAMSocket.SIMM; a < RAMSocket.NULLSK; ++a)
            {
                RAMSocketz[i] = ServiceClass.KeyValyeStringDefine(a);
                ++i;
            }
            i = 0;
            for (ProcSocket a = ProcSocket.DIP; a < ProcSocket.NULLSK; ++a)
            {
                PROCSocketz[i] = ServiceClass.KeyValyeStringDefine(a);
                ++i;
            }
            i = 0;
            for (ODDType a = ODDType.CD_ROM; a < ODDType.NULLSK; ++a)
            {
                ODDTypez[i] = ServiceClass.KeyValyeStringDefine(a);
                ++i;
            }
            i = 0;
            for (SATAType a = SATAType.SATA1; a < SATAType.NULLSK; ++a)
            {
                SATATypez[i] = ServiceClass.KeyValyeStringDefine(a);
                ++i;
            }
            #endregion
        }
    }
}
