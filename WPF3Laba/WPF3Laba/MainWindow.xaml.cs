﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CompStorage;
using System.Windows.Forms;

namespace WPF3Laba
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            //if (System.Windows.Forms.Application.MessageLoop)
            //{
            //    System.Windows.Forms.Application.Exit();
            //}
        }

        private void DragWindowAction(object sender, MouseButtonEventArgs e)
        {
            base.DragMove();
        }

        private void BtnCloseAction(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
